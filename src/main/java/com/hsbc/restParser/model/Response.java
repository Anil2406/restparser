package com.hsbc.restParser.model;

import java.util.ArrayList;

public class Response {
	private String cod;
    private float calctime;
    private int cnt;
    private ArrayList<City> list;
    
    public float getCalctime() {
		return calctime;
	}

	public void setCalctime(float calctime) {
		this.calctime = calctime;
	}

	public int getCnt() {
		return cnt;
	}

	public void setCnt(int cnt) {
		this.cnt = cnt;
	}

	public ArrayList<City> getList() {
		return list;
	}

	public void setList(ArrayList<City> list) {
		this.list = list;
	}

	
    public void setCod(String cod){	
    	this.cod = cod;
    }
    
    public String getCod(){	
    	return this.cod;
    }

    
}
