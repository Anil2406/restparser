package com.hsbc.restParser.model;

public class Coord {
private float lon;
private float lat;

public float getLon(){
	return this.lon;
}

public void setLon(float l){
	this.lon = l;
}

public float getLat(){
	return this.lat;
}

public void setLat(float lt){
	this.lat = lt;
}
}
