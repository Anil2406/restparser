package com.hsbc.restParser.model;

import java.util.ArrayList;

public class City {
	private int id;
	private String name;
	private Coord coord;
	private Main main;
	private int dt;
	private Object wind;
	private Object rain;
	private Clouds clouds;
	private ArrayList<Weather>  weather;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Coord getCoord() {
		return coord;
	}
	public void setCoord(Coord coord) {
		this.coord = coord;
	}
	public Main getMain() {
		return main;
	}
	public void setMain(Main main) {
		this.main = main;
	}
	public int getDt() {
		return dt;
	}
	public void setDt(int dt) {
		this.dt = dt;
	}
	public Object getWind() {
		return wind;
	}
	public void setWind(Object wind) {
		this.wind = wind;
	}
	public Object getRain() {
		return rain;
	}
	public void setRain(Object rain) {
		this.rain = rain;
	}
	public Clouds getClouds() {
		return clouds;
	}
	public void setClouds(Clouds clouds) {
		this.clouds = clouds;
	}
	public ArrayList<Weather> getWeather() {
		return weather;
	}
	public void setWeather(ArrayList<Weather> weather) {
		this.weather = weather;
	}
	
	
}

