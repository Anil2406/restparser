package com.hsbc.restParser;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsbc.restParser.model.City;
import com.hsbc.restParser.model.Response;


public class RestParser 
{
	HttpClient client;
	
	public RestParser() {
		this.client = HttpClientBuilder.create().build();
	}
	
	public String getCityInfo(String url) throws IOException, ClientProtocolException{		
		HttpGet request = new HttpGet(url);
		HttpResponse response = this.client.execute(request);

		return convertStringToString(response.getEntity().getContent());
	}


	public Response mapAPIResponse(String jsonPayload) throws JsonMappingException, JsonParseException, IOException{
		ObjectMapper  mapper = new ObjectMapper();
		return mapper.readValue(jsonPayload , Response.class);
	}

	public int getNumberOfCityStartWith(Response apiResponse, String startWith){

		Predicate<City> startsWith = city -> city.getName().startsWith(startWith);

		List<City> result = apiResponse.getList().stream().filter(startsWith).collect(Collectors.toList());

		return result.size();
	}
	
	public boolean writeToFile(int num) throws Exception {
		FileWriter fw=new FileWriter(".\\testout.txt");    
        fw.write("Num:"+num);    
        fw.close();
        return true;
	}

	private String convertStringToString(InputStream payload) {
		Scanner s = new  Scanner(payload).useDelimiter("\\A");
		return s.hasNext()? s.next(): "";
	}

}
