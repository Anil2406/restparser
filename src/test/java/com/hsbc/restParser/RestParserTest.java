package com.hsbc.restParser;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.hsbc.restParser.model.Response;

public class RestParserTest {
	private RestParser restParser;
	private String responseJson;
	
	@BeforeClass
	public void initRestParser() throws Exception{
		this.restParser = new RestParser();
		String url = "https://samples.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=b6907d289e10d714a6e88b30761fae22";
		this.responseJson = this.restParser.getCityInfo(url);
	}
	
	@Test
	public void getCityInfo() throws Exception {
		assertEquals(this.responseJson.contains("\"cod\":\"200\""), true);
	}
	
	@Test
	public void mapAPIResponse() throws Exception {
		assertEquals(this.restParser.mapAPIResponse(this.responseJson) instanceof Response, true);
	}
	
	@Test
	public void getNumberOfCityStartWith() throws Exception {
		Response response = this.restParser.mapAPIResponse(this.responseJson);
		assertEquals(this.restParser.getNumberOfCityStartWith(response, "T"), 2);
	}
	
	@Test
	public void writeToFile() throws Exception{
		Response response = this.restParser.mapAPIResponse(this.responseJson);
		int result= this.restParser.getNumberOfCityStartWith(response, "T");
		assertEquals(this.restParser.writeToFile(result), true);
	}
}
